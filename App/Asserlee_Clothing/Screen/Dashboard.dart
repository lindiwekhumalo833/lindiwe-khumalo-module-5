import'package:flutter/material.dart';

class DashBoard extends StatelessWidget {
  Size size;

  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    return MaterialApp(
      title: 'My_Dashboard',
      theme: ThemeData(
        primarySwatch: Colors.deeporange,
      ),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey,
          leading: Icon(Icons.menu),
          title: Text(
            "Dashboard",
            textAlign: TextAlign.center,
          ),
        ),
        body: Stack(
          children: [
            Container(
              child: CustomPaint(
                painter: ShapesPainter(),
                child: Container(
                  height: size.height / 2,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 30),
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: GridView.count(
                  crossAxisCount: 2,
                  children: [
                    createGridItem(1, 2 , 3, 4, 5),
                    
                  ],
                ),
              ),
            )
          ],
        ),
      );
  }
    
  