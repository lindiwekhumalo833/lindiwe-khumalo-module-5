MaterialApp(
  title: Asserlee,
  theme: ThemeData( 
    brightness: Brightness.dark,
    primaryColor: Colors.deepOrange[850],
 fontFamily: 'Raleway',

    textTheme: const TextTheme(
      headline3: TextStyle(fontSize: 55.0, fontWeight: FontWeight.bold),
      headline1: TextStyle(fontSize: 30.0, fontStyle: FontStyle.italic),
      bodyText2: TextStyle(fontSize: 16.0, fontFamily: 'courier'),
    ),
  ),
  home: const HomePage(
    title: Asserlee,
  ),
);
